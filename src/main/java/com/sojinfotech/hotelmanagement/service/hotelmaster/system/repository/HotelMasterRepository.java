package com.sojinfotech.hotelmanagement.service.hotelmaster.system.repository;


import com.sojinfotech.hotelmanagement.service.hotelmaster.system.entity.HotelMaster;
import org.springframework.data.repository.CrudRepository;

public interface HotelMasterRepository extends CrudRepository<HotelMaster, Integer> {

}
