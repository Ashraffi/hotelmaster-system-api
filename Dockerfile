FROM java:latest

COPY ./build/libs/*.jar /usr/app/

WORKDIR /usr/app

RUN sh -c 'touch hotelemanagement-service-0.0.1-SNAPSHOT.jar'

ENTRYPOINT ["java","-jar","hotelemanagement-service-0.0.1-SNAPSHOT.jar"]